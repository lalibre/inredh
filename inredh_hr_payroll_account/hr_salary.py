# -*- coding: utf-8 -*-

from openerp.osv import osv, fields

class hr_salary_rule(osv.osv):
    _inherit = 'hr.salary.rule'
    _columns = {
        'account_analytic': fields.boolean('Cuenta analítica en rol'),
    }

