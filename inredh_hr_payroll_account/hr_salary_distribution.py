# -*- encoding: utf-8 -*-

from openerp import fields, api, models
from openerp.exceptions import ValidationError, UserError


class hr_salary_distribution(models.Model):
    """
    Calcula la distribución del valor a pagar en sueldos a multiples cuentas analiticas y contables
    basado en un porcentaje
    """

    _name = 'hr.salary.distribution'
    _description = __doc__

    def _get_default_name(self):
        return self._name

    name = fields.Char('Nombre', _compute="_get_default_name")
    account = fields.Many2one('account.account', 'Cuenta contable')
    account_analytic = fields.Many2one('account.analytic.account', 'Cuenta analitica')
    value = fields.Selection([
            ('balance', 'Balance'),
            ('percent', 'Porcentaje')
        ], string='Tipo', required=True, default='balance',
        help="Tipo de valor que tendrá la línea de distribución.")
    value_amount = fields.Float('Porcentaje', help="El valor porcentual, ej. 50 refiere al 50%")
    hr_contract = fields.Many2one('hr.contract', 'Contrato')

    @api.one
    @api.constrains('value', 'value_amount')
    def _check_percent(self):
        if self.value == 'percent' and (self.value_amount < 0.0 or self.value_amount > 100.0):
            raise UserError('El porcentaje debe estar entre 0 y 100.')


class hr_contract(models.Model):
    _inherit = 'hr.contract'

    @api.constrains('salary_distribution_ids')
    @api.one
    def _check_lines(self):
        salary_lines = self.salary_distribution_ids.sorted()
        if salary_lines and salary_lines[-1].value != 'balance':
            raise ValidationError('Una línea de distribución salarial debe ser de balance.')
        lines = self.salary_distribution_ids.filtered(lambda r: r.value == 'balance')
        if len(lines) > 1:
            raise ValidationError('Debe haber una sola línea de distribución salarial de tipo balance.')

    salary_distribution_ids = fields.One2many('hr.salary.distribution', 'hr_contract', 'Distribución')