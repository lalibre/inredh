# -*- encoding: utf-8 -*-

from openerp import models, fields, api


class Comprobante(models.Model):
    _inherit = 'l10n_ec_sri.comprobante'

    purchase_journal_id = fields.Many2one('account.journal', 'Diario en adquisiciones', help="Diario por defecto")
    sales_journal_id = fields.Many2one('account.journal', 'Diario en ventas', help="Diario por defecto")
    active = fields.Boolean('Activo', default=True)
