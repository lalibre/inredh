# -*- encoding: utf-8 -*-

from openerp import models, fields, api, _
from openerp.exceptions import UserError


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    partner_phone = fields.Char('Partner phone', related='partner_id.phone')

    @api.onchange('comprobante_id')
    def _onchange_comprobante(self):
        if self.comprobante_id:
            if self.type == 'in_invoice' and self.comprobante_id.purchase_journal_id:
                self.journal_id = self.comprobante_id.purchase_journal_id
            elif self.type == 'out_invoice' and self.comprobante_id.sales_journal_id:
                self.journal_id = self.comprobante_id.sales_journal_id


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"
    _order = "account_code asc, date desc, id desc"

    @api.multi
    def _update_check(self):
        """ Raise Warning to cause rollback if the move is posted, some entries are reconciled or the move is older than the lock date"""
        move_ids = set()
        for line in self:
            err_msg = _('Move name (id): %s (%s)') % (line.move_id.name, str(line.move_id.id))
            if line.move_id.state != 'draft':
                raise UserError(_(
                    'You cannot do this modification on a posted journal entry, you can just change some non legal fields. You must revert the journal entry to cancel it.\n%s.') % err_msg)
            if line.reconciled and line.full_reconcile_id:
                raise UserError(_(
                    'You cannot do this modification on a reconciled entry. You can just change some non legal fields or you must unreconcile first.\n%s.') % err_msg)
            if line.move_id.id not in move_ids:
                move_ids.add(line.move_id.id)
            self.env['account.move'].browse(list(move_ids))._check_lock_date()
        return True

    account_code = fields.Char(related='account_id.code', string='Código de cuenta',
                               store=True, copy=False, help="Campo usado para ordenar")


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    @api.one
    def copy(self, default=None):
        default = dict(default or {})
        code = ''
        for num in xrange(1, 100):
            # journal_code has a maximal size of 5, hence we can enforce the boundary num < 100
            journal_code = (self.type == 'cash' and 'CSH' or 'BNK') + str(num)
            journal = self.env['account.journal'].search(
                [('code', '=', journal_code), ('company_id', '=', self.company_id.id)], limit=1)
            if not journal:
                code = journal_code
                break
        else:
            raise UserError(_("Cannot generate an unused journal code. Please fill the 'Shortcode' field."))

        default.update(
            code=code,
            name=_("%s (copy)") % (self.name or ''))
        return super(AccountJournal, self).copy(default)


# class AccountMove(models.Model):
#     _inherit = "account.move"
#
#     @api.multi
#     def unlink(self):
#         for rec in self:
#             if rec.state == 'draft':
#                 rec.line_ids.unlink()
#         pass
