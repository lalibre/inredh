# -*- coding: utf-8 -*-
{
    'name': "Modificaciones a la contablidad",
    'description': "Mejor usabilidad contable",
    'summary': """Incluye algunas funcionalidades para la la gestión

- Diarios de voucher
- Confirmación de publicación de asientos
- Diarios de compras y ventas para comprobantes

    """,
    'version': '9.0.1.0.0',
    'author': "Jonathan Finlay <jfinlay@riseup.net>",
    'maintainer': 'Jonathan Finlay <jfinlay@riseup.net>',
    'website': 'http://www.lalibre.net',
    'license': 'AGPL-3',
    'category': 'Account',
    'depends': [
        'account',
        'l10n_ec_sri'
    ],
    'data': [
        'views/account_view.xml',
        'views/account_move_view.xml',
        'views/comprobante.xml',
        'views/account_payment.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
}
