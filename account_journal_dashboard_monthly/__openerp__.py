# -*- coding: utf-8 -*-
{
    'name' : 'Account Journal Dashboard Monthly',
    'version' : '1.1',
    'summary': 'Get the account bank and cash kanban by month',
    'author': "Jonathan Finlay <jfinlay@riseup.net>",
    'maintainer': 'Jonathan Finlay <jfinlay@riseup.net>',
    'website': 'http://www.lalibre.net',
    'sequence': 30,
    'description': """
Dashboard Monthly:
======================
Transform the account journal dashboard to see Monthly, by tree months
    """,
    'category': 'Accounting & Finance',
    'website': 'http://www.lalibre.net',
    'depends': ['account'],
    'data': [
    ],
    'demo': [
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
