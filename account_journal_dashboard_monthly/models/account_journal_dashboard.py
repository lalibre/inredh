# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

from babel.dates import format_date

from openerp import models, api
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF

class account_journal(models.Model):
    _inherit = "account.journal"

    @api.multi
    def get_line_graph_datas(self):
        data = []
        today = datetime.today()
        last_month = today + timedelta(days=-90) # Change from 30
        bank_stmt = []
        # Query to optimize loading of data for bank statement graphs
        # Return a list containing the latest bank statement balance per day for the
        # last 30 days for current journal
        query = """SELECT a.date, a.balance_end
                           FROM account_bank_statement AS a,
                               (SELECT c.date, max(c.id) AS stmt_id
                                   FROM account_bank_statement AS c
                                   WHERE c.journal_id = %s
                                       AND c.date > %s
                                       AND c.date <= %s
                                       GROUP BY date, id
                                       ORDER BY date, id) AS b
                           WHERE a.id = b.stmt_id ORDER BY a.date;""" # Add order by date

        self.env.cr.execute(query, (self.id, last_month, today))
        bank_stmt = self.env.cr.dictfetchall()

        last_bank_stmt = self.env['account.bank.statement'].search(
            [('journal_id', 'in', self.ids), ('date', '<=', last_month.strftime(DF))], order="date desc, id desc",
            limit=1)
        start_balance = last_bank_stmt and last_bank_stmt[0].balance_end or 0

        locale = self._context.get('lang', 'en_US')
        show_date = last_month
        # get date in locale format
        name = format_date(show_date, 'd LLLL Y', locale=locale)
        short_name = format_date(show_date, 'd MMM', locale=locale)
        data.append({'x': short_name, 'y': start_balance, 'name': name})

        for stmt in bank_stmt:
            # fill the gap between last data and the new one
            number_day_to_add = (datetime.strptime(stmt.get('date'), DF) - show_date).days
            last_balance = data[len(data) - 1]['y']
            for day in range(0, number_day_to_add + 1):
                show_date = show_date + timedelta(days=1)
                # get date in locale format
                name = format_date(show_date, 'd LLLL Y', locale=locale)
                short_name = format_date(show_date, 'd MMM', locale=locale)
                data.append({'x': short_name, 'y': last_balance, 'name': name})
            # add new stmt value
            data[len(data) - 1]['y'] = stmt.get('balance_end')

        # continue the graph if the last statement isn't today
        if show_date != today:
            number_day_to_add = (today - show_date).days
            last_balance = data[len(data) - 1]['y']
            for day in range(0, number_day_to_add):
                show_date = show_date + timedelta(days=1)
                # get date in locale format
                name = format_date(show_date, 'd LLLL Y', locale=locale)
                short_name = format_date(show_date, 'd MMM', locale=locale)
                data.append({'x': short_name, 'y': last_balance, 'name': name})

        return [{'values': data, 'area': True}]