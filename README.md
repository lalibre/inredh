Módulos para la gestión de organizaciones sociales sin fines de lucro
=====================================================================

Incluye:
--------

- Contabilidad para Ecuador -> Movido a https://gitlab.com/jfinlay/l10n-ecuador
- Roles de pago Ecuador -> Movido a https://gitlab.com/jfinlay/l10n-ecuador
- Gestión de presupuestos por proyectos
- Gestión analítica de roles
- Computo de saldos de cuentas por proyectos

Usar `inredh_cep_onclick_installer` para instalar todos los módulos de manera automática.
-----------------------------------------------------------------------------------------

Para implemetar un nuevo plan contable se deben crear 4 módulos:

`l10n_ec_*_base` : Implementa el plan contable
`extra_*_sri_map` : Implementa el mapeo de impuestos
`extra_*_payroll_account_map` : Implementa el mapeo de reglas 
`inredh_*click_installer` : Metapaquete que instala dependencias necesarias

