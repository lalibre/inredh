# -*- encoding: utf-8 -*-

from openerp import models, fields, api


class AccountMove(models.Model):
    _inherit = 'account.move'

    proyecto_id = fields.Many2one(
        'inredh_analytic.proyecto', string='Proyecto')

class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    @api.multi
    @api.depends(
        'move_id', 'move_id.proyecto_id', 'payment_id.proyecto_id',
        'move_id.state', 'analytic_account_id',
        'analytic_account_id.proyecto_id', 'to_compute',
        'invoice_id.proyecto_id')
    def _get_proyecto(self):
        for r in self:
            if r.analytic_account_id and r.analytic_account_id.proyecto_id:
                r.proyecto_id = r.analytic_account_id.proyecto_id.id
            elif r.invoice_id and r.invoice_id.proyecto_id:
                r.proyecto_id = r.invoice_id.proyecto_id.id
            elif r.payment_id and r.payment_id.proyecto_id:
                r.proyecto_id = r.payment_id.proyecto_id.id
            elif r.move_id and r.move_id.proyecto_id:
                r.proyecto_id = r.move_id.proyecto_id.id
            else:
                # Search slip move
                slip = r.env['hr.payslip'].search([('move_id', '=', r.move_id.id)])
                if slip:
                    r.proyecto_id = slip.proyecto_id.id
                else:
                    r.proyecto_id = False

    proyecto_id = fields.Many2one(
        'inredh_analytic.proyecto', string='Proyecto', compute='_get_proyecto', store=True)
    to_compute = fields.Boolean('A computar', help="Control para computar proyecto")


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    proyecto_id = fields.Many2one(
        'inredh_analytic.proyecto', string='Proyecto',)
    proyecto_name = fields.Char('Nombre proyecto', related='proyecto_id.name', store=True)


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    proyecto_id = fields.Many2one(
        'inredh_analytic.proyecto', string='Proyecto',)


class AccountRegisterPayments(models.Model):
    _inherit = 'account.register.payments'

    proyecto_id = fields.Many2one(
        'inredh_analytic.proyecto', string='Proyecto',)
