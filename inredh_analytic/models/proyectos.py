# -*- coding: utf-8 -*-

from openerp import models, fields

class proyectos(models.Model):
    _name = 'inredh_analytic.proyecto'
    _description = "Proyectos"

    name = fields.Char('Nombre', size=64, required=True)

