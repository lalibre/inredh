# -*- encoding: utf-8 -*-

from openerp import models, fields, api, _
from openerp.tools import float_compare, float_is_zero
from openerp.exceptions import UserError
import time


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    @api.multi
    @api.depends('analytic_account_id', 'move_id')
    def _get_proyecto(self):
        for slip in self:
            if slip.analytic_account_id and slip.analytic_account_id.proyecto_id:
                slip.proyecto_id = slip.analytic_account_id.proyecto_id.id
                if slip.move_id:
                    self._cr.execute('UPDATE account_move SET proyecto_id = %s where id = %s' % (
                        slip.proyecto_id.id, slip.move_id.id))

    proyecto_id = fields.Many2one(
        'inredh_analytic.proyecto', string='Proyecto', compute="_get_proyecto")
