# -*- encoding: utf-8 -*-

from openerp import models, fields


class AccountAnaliticAccount(models.Model):
    _inherit = 'account.analytic.account'

    proyecto_id = fields.Many2one('inredh_analytic.proyecto', string='Proyecto', required=True)
