# -*- coding: utf-8 -*-
{
    'name': "Modificaciones a la contabilidad analítica",
    'description': """Incluye algunas funcionalidades para la la gestión presupuestaria para Org. Sociales.

- Añade proyectos
- Modifica el tablero de Movimientos

    """,
    'summary': "Implemente reportes por proyecto/centro de costo",
    'version': '9.0.1.0.0',
    'author': "Jonathan Finlay <jfinlay@riseup.net>",
    'maintainer': 'Jonathan Finlay <jfinlay@riseup.net>',
    'website': 'http://www.lalibre.net',
    'license': 'AGPL-3',
    'category': 'Account',
    'depends': [
        'account',
        'analytic',
        'l10n_ec_account',
        'inredh_hr_payroll_account'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/proyecto.xml',
        'views/account_analytic_account.xml',
        'views/slip_view.xml',
        'views/account_view.xml',
        'views/report_generalledger.xml',
        'reports/account_move_line_report_view.xml',
        'wizard/account_report_general_ledger_view.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
}
