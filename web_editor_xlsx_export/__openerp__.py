# -*- encoding: utf-8 -*-

{
    'name': "Web Editor XLSX Export",
    'description': "Export html table to XLSX",
    'summary': """Improve xlsx exportation of html reports
    """,
    'version': '9.0.1.0.0',
    'author': "Jonathan Finlay <jfinlay@riseup.net>",
    'maintainer': 'Jonathan Finlay <jfinlay@riseup.net>',
    'website': 'http://www.cyberzen.ec',
    'license': 'LGPL-3',
    'category': 'Web',
    'depends': [
        'web_editor',
    ],
    'qweb': [
        'static/src/xml/editor.xml'
    ],
    'js': [
        'static/src/js/editor.js'
    ],
    'installable': True,
    'web': True,
}
