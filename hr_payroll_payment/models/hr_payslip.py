# -*- coding: utf-8 -*-
import json
from openerp.tools import float_is_zero, float_compare

from openerp import models, fields, api, _


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    @api.model
    def _compute_pay_amount(self):
        line_obj = self.env['hr.payslip.line']
        liquido = line_obj.search([('code', '=', 'LIQ'), ('slip_id', '=', self.id)])
        self.amount_total = liquido.amount

    @api.multi
    def pay_sheet(self):
        slip_ids = []
        valor = self.amount_total
        account_payable_id = self.employee_id.account_sueldos_id or self.employee_id.address_home_id.property_account_payable_id
        payment_form = self.env.ref('account.view_account_payment_form', False)
        slip_ids.append(self.id)
        ctx = {
            'default_model': 'hr.payslip',
            'default_partner_id': self.employee_id.address_home_id.id,
            'default_payment_type': 'outbound',
            'default_res_id': self.id,
            'default_amount': valor,
            'default_contrapartida_id': account_payable_id.id,
            'default_communication': self.number,
            'default_payment_reference': self.name,
            'default_slip_ids': [(6, 0, slip_ids)],

        }
        print ctx
        return {
            'name': 'Pagar rol',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.payment',
            'views': [(payment_form.id, 'form')],
            'view_id': payment_form.id,
            'target': 'new',
            'context': ctx,
        }

    # TODO: refund_sheet: Control de estado del asiento de pago,
    # TODO: refund_sheet: depuración de error actual

    @api.model
    def _compute_payment(self):
        payment_obj = self.env['account.payment']
        self.payment_id = payment_obj.search([('payroll_slip_id', '=', self.id)])

    @api.model
    def _compute_paid_state(self):
        self.paid = False
        if not self.residual:
            self.paid = True

    @api.model
    def _default_currency(self):
        return self.env.user.company_id.currency_id

    @api.one
    @api.depends(
        'state', 'currency_id',
        'move_id.line_ids.amount_residual',
        'move_id.line_ids.currency_id',
        'residual')
    def _compute_residual(self):
        residual = 0.0
        residual_company_signed = 0.0
        sign = self.credit_note and -1 or 1
        for line in self.sudo().move_id.line_ids:
            if line.account_id.internal_type in ('receivable', 'payable'):
                residual_company_signed += line.amount_residual
                if line.currency_id == self.currency_id:
                    residual += line.amount_residual_currency if line.currency_id else line.amount_residual
                else:
                    from_currency = (line.currency_id and line.currency_id.with_context(
                        date=line.date)) or line.company_id.currency_id.with_context(date=line.date)
                    residual += from_currency.compute(line.amount_residual, self.currency_id)
        self.residual_company_signed = abs(residual_company_signed) * sign
        self.residual_signed = abs(residual) * sign
        self.residual = abs(residual)
        digits_rounding_precision = self.currency_id.rounding
        if float_is_zero(self.residual, precision_rounding=digits_rounding_precision):
            self.reconciled = True
        else:
            self.reconciled = False

    @api.one
    def _get_outstanding_info_JSON(self):
        self.outstanding_credits_debits_widget = json.dumps(False)
        if self.state == 'done':
            domain = [('account_id', '=', self.employee_id.account_sueldos_id.id or self.employee_id.address_home_id.property_account_payable_id.id),
                      ('partner_id', '=', self.employee_id.address_home_id.id),
                      ('reconciled', '=', False), ('amount_residual', '!=', 0.0)]
            if self.credit_note:
                domain.extend([('credit', '>', 0), ('debit', '=', 0)])
                type_payment = _('Outstanding credits')
            else:
                domain.extend([('credit', '=', 0), ('debit', '>', 0)])
                type_payment = _('Outstanding debits')
            info = {'title': '', 'outstanding': True, 'content': [], 'slip_id': self.id}
            lines = self.env['account.move.line'].search(domain)
            currency_id = self.currency_id
            if len(lines) != 0:
                for line in lines:
                    # get the outstanding residual value in slip currency
                    if line.currency_id and line.currency_id == self.currency_id:
                        amount_to_show = abs(line.amount_residual_currency)
                    else:
                        amount_to_show = line.company_id.currency_id.with_context(date=line.date).compute(
                            abs(line.amount_residual), self.currency_id)
                    if float_is_zero(amount_to_show, precision_rounding=self.currency_id.rounding):
                        continue
                    info['content'].append({
                        'journal_name': line.ref or line.move_id.name,
                        'amount': amount_to_show,
                        'currency': currency_id.symbol,
                        'id': line.id,
                        'position': currency_id.position,
                        'digits': [69, self.currency_id.decimal_places],
                    })
                info['title'] = type_payment
                self.outstanding_credits_debits_widget = json.dumps(info)
                self.has_outstanding = True

    @api.one
    @api.depends('payment_move_line_ids.amount_residual')
    def _get_payment_info_JSON(self):
        self.payments_widget = json.dumps(False)
        if self.payment_move_line_ids:
            info = {'title': _('Less Payment'), 'outstanding': False, 'content': []}
            currency_id = self.currency_id
            for payment in self.payment_move_line_ids:
                payment_currency_id = False
                amount = sum(
                    [p.amount for p in payment.matched_credit_ids if p.credit_move_id in self.move_id.line_ids])
                amount_currency = sum([p.amount_currency for p in payment.matched_credit_ids if
                                       p.credit_move_id in self.move_id.line_ids])
                if payment.matched_credit_ids:
                    payment_currency_id = all([p.currency_id == payment.matched_credit_ids[0].currency_id for p in
                                               payment.matched_credit_ids]) and payment.matched_credit_ids[0].currency_id or False
                # get the payment value in slip currency
                if payment_currency_id and payment_currency_id == self.currency_id:
                    amount_to_show = amount_currency
                else:
                    amount_to_show = payment.company_id.currency_id.with_context(date=payment.date).compute(amount,
                                                                                                            self.currency_id)
                if float_is_zero(amount_to_show, precision_rounding=self.currency_id.rounding):
                    continue
                payment_ref = payment.move_id.name
                if payment.move_id.ref:
                    payment_ref += ' (' + payment.move_id.ref + ')'
                info['content'].append({
                    'name': payment.name,
                    'journal_name': payment.journal_id.name,
                    'amount': amount_to_show,
                    'currency': currency_id.symbol,
                    'digits': [69, currency_id.decimal_places],
                    'position': currency_id.position,
                    'date': payment.date,
                    'payment_id': payment.id,
                    'move_id': payment.move_id.id,
                    'ref': payment_ref,
                })
            self.payments_widget = json.dumps(info)

    @api.one
    @api.depends('move_id.line_ids.amount_residual')
    def _compute_payments(self):
        payment_lines = []
        for line in self.move_id.line_ids:
            payment_lines.extend(filter(None, [rp.credit_move_id.id for rp in line.matched_credit_ids]))
            payment_lines.extend(filter(None, [rp.debit_move_id.id for rp in line.matched_debit_ids]))
        self.payment_move_line_ids = self.env['account.move.line'].browse(list(set(payment_lines)))

    @api.multi
    def register_payment(self, payment_line, writeoff_acc_id=False, writeoff_journal_id=False):
        """ Reconcile payable/receivable lines from the invoice with payment_line """
        line_to_reconcile = self.env['account.move.line']
        for slip in self:
            line_to_reconcile += slip.move_id.line_ids.filtered(lambda r: not r.reconciled and r.account_id.internal_type in ('payable', 'receivable'))
        return (line_to_reconcile + payment_line).reconcile(writeoff_acc_id, writeoff_journal_id)

    @api.v7
    def assign_outstanding_credit(self, cr, uid, id, credit_aml_id, context=None):
        credit_aml = self.pool.get('account.move.line').browse(cr, uid, credit_aml_id, context=context)
        slip = self.browse(cr, uid, id, context=context)
        if not credit_aml.currency_id and slip.currency_id != slip.company_id.currency_id:
            credit_aml.with_context(allow_amount_currency=True).write({
                'amount_currency': slip.company_id.currency_id.with_context(date=credit_aml.date).compute(credit_aml.balance, slip.currency_id),
                'currency_id': slip.currency_id.id})
        if credit_aml.payment_id:
            credit_aml.payment_id.write({'slip_ids': [(4, id, None)]})
        return slip.register_payment(credit_aml)


    amount_total = fields.Float(
        'Liquido a pagar', help='El valor a pagar al empleado',
        compute='_compute_pay_amount')
    paid = fields.Boolean(
        'Made Payment Order ? ', required=False, readonly=True, states={'draft': [('readonly', False)]},
        copy=False, compute="_compute_paid_state")
    reconciled = fields.Boolean(
        'Pagado/Conciliado', copy=False, compute="_compute_residual", help="Indica sí el rol ha sido conciliado")
    payment_id = fields.Many2one(
        'account.payment', 'Pago', compute="_compute_payment")
    # Full payment functionality
    residual = fields.Monetary(
        string='Amount Due',compute='_compute_residual', store=True, help="Remaining amount due.")
    residual_signed = fields.Monetary(
        string='Amount Due', currency_field='currency_id', compute='_compute_residual', store=True,
        help="Remaining amount due in the currency of the slip.")
    residual_company_signed = fields.Monetary(
        string='Amount Due', currency_field='company_currency_id', compute='_compute_residual', store=True,
        help="Remaining amount due in the currency of the company.")
    payment_ids = fields.Many2many(
        'account.payment', 'slip_payment_rel', 'slip_id', 'payment_id', string="Payments", copy=False, readonly=True)
    payment_move_line_ids = fields.Many2many(
        'account.move.line', string='Payments', compute='_compute_payments', store=True)
    payments_widget = fields.Text(compute='_get_payment_info_JSON')
    outstanding_credits_debits_widget = fields.Text(compute='_get_outstanding_info_JSON')
    has_outstanding = fields.Boolean(compute='_get_outstanding_info_JSON')
    # Multicurrency
    currency_id = fields.Many2one('res.currency', string='Currency',
                                  required=True, readonly=True, states={'draft': [('readonly', False)]},
                                  default=_default_currency, track_visibility='always')
    company_currency_id = fields.Many2one('res.currency', related='company_id.currency_id', readonly=True)
    company_id = fields.Many2one('res.company', string='Company', change_default=True,
                             required=True, readonly=True, states={'draft': [('readonly', False)]},
                             default=lambda self: self.env['res.company']._company_default_get('account.invoice'))




