# -*- coding: utf-8 -*-
from openerp import models, fields, api, _


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    @api.onchange('prepayment')
    def _onchange_prepayment(self):
        if not self.prepayment and not self.payroll_slip_id:
            self.contrapartida_id = False

    @api.one
    @api.depends('invoice_ids', 'slip_ids')
    def _get_has_invoices(self):
        self.has_invoices = bool(self.invoice_ids) or bool(self.slip_ids)

    payroll_slip_id = fields.Many2one('hr.payslip')
    slip_ids = fields.Many2many('hr.payslip', 'hr_payslip_payment_rel', 'payment_id', 'slip_id',
                                   string="Slips", copy=False, readonly=True)
    has_invoices = fields.Boolean(compute="_get_has_invoices", help="Technical field used for usability purposes")

