# -*- coding: utf-8 -*-

from openerp import models, fields, api

import xlwt
from cStringIO import StringIO
import base64


class AccountTransactionalReport(models.TransientModel):
    _name = 'account.transactional.report'
    _description = 'Account Transactional Report'

    date = fields.Date('Date from', required=True)
    date_to = fields.Date('Date to', required=True)
    data = fields.Binary('Excel report')
    name = fields.Char('File name', size=64)
    type = fields.Selection(
        [('in_invoice', 'Purchase invoice'),
         ('out_invoice', 'Sale invoice')], 'Type', required=True)

    @api.constrains('date', 'date_to')
    def _check_dates(self):
        if self.date > self.date_to:
            raise UserWarning("Date from can't be greater than date to")

    def _get_query(self):
        queries = []
        purchase_invoice = u"""
SELECT
  account_invoice.date_invoice AS "FECHA DE FACTURA",
  l10n_ec_sri_detercero.puntoemision as "EMISION",
  l10n_ec_sri_detercero.establecimiento as "SERIE",
  l10n_ec_sri_detercero.name as "AUTORIZACION",
  account_invoice."secuencial" AS "NÚMERO DE FACTURA",
  account_invoice."number" AS "CÓDIGO EN SISTEMA",
  res_partner.vat_ec AS "CI/RUC",
  res_partner.name AS "EMPRESA",
  account_invoice.amount_untaxed AS "BASE",
  account_move.amount AS "TOTAL FACTURA",
  (
  select sum(amount)
  from account_invoice_tax
  where invoice_id = account_invoice.id and amount > 0
  ) as "IVA",
  account_invoice.r_secuencial as "NUM. RETENCIÓN",
  (
  select sum(account_invoice_tax.amount)
  from account_invoice_tax, account_tax, account_tax_group
  where account_tax.tax_group_id = account_tax_group.id
  and account_tax_group.name = 'RetIr'
  and account_tax.id = account_invoice_tax.tax_id
  and invoice_id = account_invoice.id
  and account_invoice_tax.amount < 0
  ) as "RET. IR",
  (
  select sum(account_invoice_tax.amount)
  from account_invoice_tax, account_tax, account_tax_group
  where account_tax.tax_group_id = account_tax_group.id
  and account_tax_group.name in ('RetIva','RetBien10','RetBienes','RetServ100','RetServ20','RetServicios')
  and account_tax.id = account_invoice_tax.tax_id
  and invoice_id = account_invoice.id
  and account_invoice_tax.amount < 0
  ) as "RET. IVA",
  (
  select sum(account_invoice_tax.amount)
  from account_invoice_tax, account_tax
  where account_tax.id = account_invoice_tax.tax_id
  and invoice_id = account_invoice.id
  and account_invoice_tax.amount < 0
  ) as "TOTAL RETENIDO",
  account_invoice.proyecto_name AS "PROYECTO"
FROM
  public.account_invoice,
  public.res_partner,
  public.account_move,
  public.l10n_ec_sri_detercero
WHERE
  account_invoice.partner_id = res_partner.id AND
  account_invoice.move_id = account_move.id AND
  account_invoice.state in ('paid', 'open') AND
  account_invoice.type = '%s' AND
  account_invoice.c_detercero_id = l10n_ec_sri_detercero.id AND
  account_invoice.date_invoice BETWEEN '%s' AND '%s'
ORDER BY
  account_invoice."date_invoice" ASC,
  account_invoice."number" ASC;
            """ % (self.type, fields.Date.from_string(self.date), fields.Date.from_string(self.date_to))

        liquidation_invoice = u"""
SELECT
  account_invoice.date_invoice AS "FECHA DE FACTURA",
  l10n_ec_sri_autorizacion.puntoemision as "EMISION",
  l10n_ec_sri_autorizacion.establecimiento as "SERIE",
  l10n_ec_sri_autorizacion.name as "AUTORIZACION",
  account_invoice."secuencial" AS "NÚMERO DE FACTURA",
  account_invoice."number" AS "CÓDIGO EN SISTEMA",
  res_partner.vat_ec AS "CI/RUC",
  res_partner.name AS "EMPRESA",
  account_invoice.amount_untaxed AS "BASE",
  account_move.amount AS "TOTAL FACTURA",
  (
  select sum(amount)
  from account_invoice_tax
  where invoice_id = account_invoice.id and amount > 0
  ) as "IVA",
  account_invoice.r_secuencial as "NUM. RETENCIÓN",
  (
  select sum(account_invoice_tax.amount)
  from account_invoice_tax, account_tax, account_tax_group
  where account_tax.tax_group_id = account_tax_group.id
  and account_tax_group.name = 'RetIr'
  and account_tax.id = account_invoice_tax.tax_id
  and invoice_id = account_invoice.id
  and account_invoice_tax.amount < 0
  ) as "RET. IR",
  (
  select sum(account_invoice_tax.amount)
  from account_invoice_tax, account_tax, account_tax_group
  where account_tax.tax_group_id = account_tax_group.id
  and account_tax_group.name in ('RetIva','RetBien10','RetBienes','RetServ100','RetServ20','RetServicios')
  and account_tax.id = account_invoice_tax.tax_id
  and invoice_id = account_invoice.id
  and account_invoice_tax.amount < 0
  ) as "RET. IVA",
  (
  select sum(account_invoice_tax.amount)
  from account_invoice_tax, account_tax
  where account_tax.id = account_invoice_tax.tax_id
  and invoice_id = account_invoice.id
  and account_invoice_tax.amount < 0
  ) as "TOTAL RETENIDO",
  account_invoice.proyecto_name AS "PROYECTO"
FROM
  public.account_invoice,
  public.res_partner,
  public.account_move,
  public.l10n_ec_sri_autorizacion
WHERE
  account_invoice.partner_id = res_partner.id AND
  account_invoice.move_id = account_move.id AND
  account_invoice.state in ('paid', 'open') AND
  account_invoice.type = '%s' AND
  account_invoice.c_autorizacion_id = l10n_ec_sri_autorizacion.id AND
  account_invoice.date_invoice BETWEEN '%s' AND '%s'
ORDER BY
  account_invoice."date_invoice" ASC,
  account_invoice."number" ASC;
                    """ % (self.type, fields.Date.from_string(self.date), fields.Date.from_string(self.date_to))

        queries.append(purchase_invoice)
        queries.append(liquidation_invoice)
        # queries.append(sale_invoice)
        return queries

    @api.multi
    def print_excel_report(self):
        filename = 'Reporte transaccional.xls'
        workbook = xlwt.Workbook(encoding="UTF-8")
        worksheet = workbook.add_sheet('Report')

        # Styles
        HEADER = xlwt.easyxf('alignment: horiz centre; font: bold on;')
        FIELDS = xlwt.easyxf('font: bold on')
        THEADER = xlwt.easyxf('alignment: horiz centre; font: bold on; '
                              'borders: left thin, top thin, bottom thin, right thin')
        # Print header
        worksheet.write_merge(0, 0, 0, 15, 'REPORTE DE TRANSACCIONES', HEADER)

        worksheet.write(3, 0, "Desde", FIELDS)
        worksheet.write(3, 1, self.date)
        worksheet.write(3, 2, "-", FIELDS)
        worksheet.write(3, 3, self.date_to)

        # Print table header

        worksheet.write(5, 0, "FECHA DE FACTURA", THEADER)
        worksheet.write(5, 1, "EMISION", THEADER)
        worksheet.write(5, 2, "SERIE", THEADER)
        worksheet.write(5, 3, "AUTORIZACION", THEADER)
        worksheet.write(5, 4, "NÚMERO DE FACTURA", THEADER)
        worksheet.write(5, 5, "CÓDIGO EN SISTEMA", THEADER)
        worksheet.write(5, 6, "CI/RUC", THEADER)
        worksheet.write(5, 7, "EMPRESA", THEADER)
        worksheet.write(5, 8, "BASE", THEADER)
        worksheet.write(5, 9, "TOTAL FACTURA", THEADER)
        worksheet.write(5, 10, "IVA", THEADER)
        worksheet.write(5, 11, "NUM. RETENCIÓN", THEADER)
        worksheet.write(5, 12, "RET. IR", THEADER)
        worksheet.write(5, 13, "RET. IVA", THEADER)
        worksheet.write(5, 14, "TOTAL RETENIDO", THEADER)
        worksheet.write(5, 15, "PROYECTO", THEADER)

        xl_row = 6
        for query in self._get_query():
            self._cr.execute(query)
            for row in self._cr.fetchall():
                xl_col = 0
                for field in row:
                    worksheet.write(xl_row, xl_col, field)
                    xl_col += 1
                xl_row += 1
        fp = StringIO()
        workbook.save(fp)
        self.write({'data': base64.encodestring(fp.getvalue()), 'name': filename})
        return {
            'name': 'Transactional Report',
            'res_model': 'account.transactional.report',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'nodestroy': True,
            'context': self._context,
            'res_id': self.id,
        }