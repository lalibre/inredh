# -*- coding: utf-8 -*-
{
    'name' : 'Account Transactional Reports',
    'version' : '1.1',
    'summary': 'Transactional xlsx Reports',
    'author': "Jonathan Finlay <jfinlay@riseup.net>",
    'maintainer': 'Jonathan Finlay <jfinlay@riseup.net>',
    'website': 'http://www.lalibre.net',
    'sequence': 30,
    'description': """
Transactional reports:
======================
Provides transactional reports for SRI
    """,
    'category' : 'Accounting & Finance',
    'website': 'http://www.lalibre.net',
    'depends' : ['account', 'l10n_ec_sri', 'inredh_analytic'],
    'data': [
        'wizard/account_transactional_report_view.xml',
    ],
    'demo': [
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
