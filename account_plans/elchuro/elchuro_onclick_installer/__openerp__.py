# -*- coding: utf-8 -*-
{
    'name': "OnClick Installer - Fundación El Churo",

    'summary': """
        Instala todo lo necesario en un click""",

    'description': """
Contiene el pland e cuentas diseñado por INREDH para organizaciones sociales
    """,

    'author': "jfinlay@riseup.net",
    'website': "http://www.lalibre.net",

    'category': 'Account',
    'version': '1.0',
    'sequence': '100',
    'depends': ['analytic',
                'account_budget',
                'account_accountant',
                'l10n_ec_sri',
                'l10n_ec_payment',
                'l10n_ec_hr_payroll',
                'l10n_ec_account',
                'l10n_ec_analytic',
                'inredh_hr_payroll_account',
                'inredh_reports',
                'usability_account',
                'account_xlsx_report',
                'inredh_account_budget',
                'account_cancel',
                'web_custom_usermenu',
                'extra_ec_ats_checker',
                'l10n_ec_hr_payroll_payment',
                'inredh_bank_statement',
                'account_bank_ledger',
                'hr_payroll_cancel',
                'inredh_analytic',
                # Especificos contabilidad
                'l10n_ec_elchuro_base',
                'extra_elchuro_sri_map',
                'extra_elchuro_payroll_account_map',
                ],

    'data': [],
    'demo': [],
    'application': False,
    'installable': False,
}