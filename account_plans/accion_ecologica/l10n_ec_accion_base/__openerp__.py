# -*- coding: utf-8 -*-
{
    'name': "Plan de cuentas - Accion Ecológica",
    'description': """Agrega el plan de cuentas.""",
    'summary': """Agrega el plan de cuentas base.""",
    'version': '9.0.1.0.0',
    'author': "Jonathan Finlay",
    'maintainer': 'Jonathan Finlay',
    'website': 'http://www.lalibre.net',
    'license': 'AGPL-3',
    'category': 'Account',
    'depends': [
        'base',
        'account',
    ],
    'data': [
        'data/account_chart_template.xml',
        'data/account.account.template.csv',
        'data/account_chart_template.yml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
}
