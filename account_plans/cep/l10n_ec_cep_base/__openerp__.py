# -*- coding: utf-8 -*-
{
    'name': "Ecuador - INREDH/CEP",
    'description': """Agrega el plan de cuentas base 'INREDH/CEP'.""",
    'summary': """Agrega el plan de cuentas base 'INREDH/CEP'.""",
    'version': '9.0.1.0.0',
    'author': "Jonathan Finlay",
    'maintainer': 'Jonathan FInlay',
    'website': 'http://www.lalibre.net',
    'license': 'AGPL-3',
    'category': 'Account',
    'depends': [
        'base',
        'account',
    ],
    'data': [
        'data/account_chart_template.xml',
        'data/account.account.template.csv',
        'data/account_chart_template.yml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
}
