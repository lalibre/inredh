# -*- coding: utf-8 -*-
{
    'name': "Mapeo de impuestos INREDH",
    'summary': """Mapeo de impuestos en base al plan contable generico de INREDH.""",
    'version': '9.0.1.0.0',
    'author': "Jonathan Finlay <jfinlay@riseup.net>",
    'maintainer': 'Jonathan Finlay <jfinlay@riseup.net>',
    'website': 'http://www.lalibre.net',
    'license': 'AGPL-3',
    'category': 'Account',
    'depends': [
        'base',
        'account',
        'l10n_ec_sri',
        'l10n_ec_inredh_base'
    ],
    'data': [
        # "data/l10n_ec_sri.101_map_account.xml", No hacen ventas.
        "data/l10n_ec_sri.103_map_account.xml",
        "data/l10n_ec_sri.104_map_account.xml", # Se comentan los impuestos de venta
        "data/account.fiscal.position.map_account.xml",
    ],
    'demo': [],
    'test': [],
    'installable': True,
}
