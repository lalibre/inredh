Uso de planes:
==============

cep: Utilizado en capacitaciones
inredh: Utilizado por algunas organizaciones ej. CONAIE
plan_inredh: Utilizado por Fundación Inredh
elchuro: Utilizado por Fundación El Churo
accion_ecologica: Utilizado por Acción Ecológica

Descripción de módulos:
-----------------------

inredh_*_oneclick_installer: Instalador
l10n_ec_*_base: Plan contable
extra_*_payroll_account_map: Mapeo de reglas salariales
extra_*_sri_map: Mapeo de impuestos
