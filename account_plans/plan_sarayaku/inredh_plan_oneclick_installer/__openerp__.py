# -*- coding: utf-8 -*-
{
    'name': "OnClick Installer - Plan INREDH",

    'summary': """
        Instala todo lo necesario en un click""",

    'description': """
Contiene el plan de cuentas utilizado por INREDH
    """,

    'author': "jfinlay@riseup.net",
    'website': "http://www.lalibre.net",

    'category': 'Account',
    'version': '1.0',
    'sequence': '1',
    'depends': ['analytic',
                'account_budget',
                'account_accountant',
                'l10n_ec_sri',
                'l10n_ec_payment',
                'l10n_ec_account',
                'l10n_ec_analytic',
                'l10n_ec_plan_inredh_base',
                'extra_plan_inredh_sri_map',
                'inredh_reports',
                'usability_account',
                'account_xlsx_report',
                'inredh_account_budget',
                'account_cancel',
                'web_custom_usermenu',
                'extra_ec_ats_checker',
                'inredh_bank_statement',
                'account_bank_ledger',
                'inredh_analytic',
                ],

    'data': [],
    'demo': [],
    'application': True,
}