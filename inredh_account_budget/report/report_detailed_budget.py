# -*- encoding: utf-8 -*-
from openerp.addons.report_xlsx.report.report_xlsx import ReportXlsx

class detailed_crossovered_budget_xlsx(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, budgets):
        #Styles
        bold = workbook.add_format({'bold': True})
        # Header table
        table_header = workbook.add_format({'bold': True})
        table_header.set_bottom()
        table_header.set_top()
        table_header.set_left()
        table_header.set_right()
        # Table body
        table_body = workbook.add_format({})
        table_body.set_bottom()
        table_body.set_top()
        table_body.set_left()
        table_body.set_right()

        for obj in budgets:
            report_name = obj.name
            # One sheet by budget
            sheet = workbook.add_worksheet(report_name[:31])
            sheet.write(0, 0, obj.name, bold)
            sheet.write(1, 0, "Periodo:", bold)
            sheet.write(1, 1, "%s - %s" %(obj.date_from, obj.date_to))

            # Table Header
            sheet.write(3, 0, u"PROYECTO", table_header)
            sheet.write(3, 1, u"CUENTA ANALÍTICA", table_header)
            sheet.write(3, 2, u"EMPRESA", table_header)
            sheet.write(3, 3, u"CI/RUC", table_header)
            sheet.write(3, 4, u"CÓDIGO FAC.", table_header)
            sheet.write(3, 5, u"NÚMERO FAC.", table_header)
            sheet.write(3, 6, u"FECHA DE FAC.", table_header)
            sheet.write(3, 7, u"NÚMERO DE PAGO", table_header)
            sheet.write(3, 8, u"FECHA DE PAGO", table_header)
            sheet.write(3, 9, u"DETALLE", table_header)
            sheet.write(3, 10, u"TOTAL DETALLE", table_header)
            sheet.write(3, 11, u"TOTAL FACTURA", table_header)
            sheet.write(3, 12, u"TOTAL IMPUESTOS", table_header)
            sheet.write(3, 13, u"RET. IR", table_header)
            sheet.write(3, 14, u"RET. IVA", table_header)

            # Table Body
            row = 4
            cr = self.env.cr
            cr.execute('''
SELECT inredh_analytic_proyecto.name AS "PROYECTO",
  account_analytic_account.code AS "CÓDIGO ANALÍTICA",
  account_analytic_account.name AS "CUENTA ANALÍTICA",
  res_partner.name AS "EMPRESA",
  res_partner.vat_ec AS "CI/RUC",
  account_invoice."number" AS "CÓDIGO DE FACTURA",
  account_invoice."number" AS "NÚMERO DE FACTURA",
  account_invoice.date_invoice AS "FECHA DE FACTURA",
  account_payment.name AS "NÚMERO DE PAGO",
  account_payment.payment_date AS "FECHA PAGO",
  account_move_line.name AS "DETALLE",
  account_move_line.balance AS "TOTAL DETALLE",
  account_move.amount AS "TOTAL FACTURA",
  (select sum(amount) from account_invoice_tax where invoice_id = account_invoice.id and amount > 0) as "TOTAL IMPUESTOS",
  (
	select sum(account_invoice_tax.amount)
	from account_invoice_tax, account_tax, account_tax_group
	where account_tax.tax_group_id = account_tax_group.id
		and account_tax_group.name = 'RetIr'
		and account_tax.id = account_invoice_tax.tax_id
		and invoice_id = account_invoice.id
		and account_invoice_tax.amount < 0
  ) as "TOTAL RET. IR",
  (
	select sum(account_invoice_tax.amount)
	from account_invoice_tax, account_tax, account_tax_group
	where account_tax.tax_group_id = account_tax_group.id
		and account_tax_group.name in ('RetIva','RetBien10','RetBienes','RetServ100','RetServ20','RetServicios')
		and account_tax.id = account_invoice_tax.tax_id
		and invoice_id = account_invoice.id
		and account_invoice_tax.amount < 0
  ) as "TOTAL RET. IVA"
FROM
  public.account_invoice,
  public.account_invoice_payment_rel,
  public.account_payment,
  public.res_partner,
  public.account_analytic_account,
  public.inredh_analytic_proyecto,
  public.account_move,
  public.account_move_line
WHERE
  account_invoice.partner_id = res_partner.id AND
  account_invoice.move_id = account_move.id AND
  account_invoice_payment_rel.payment_id = account_payment.id AND
  account_invoice_payment_rel.invoice_id = account_invoice.id AND
  account_analytic_account.proyecto_id = inredh_analytic_proyecto.id AND
  account_move.id = account_move_line.move_id AND
  account_move_line.analytic_account_id = account_analytic_account.id AND
  account_invoice.state = 'paid' AND
  account_payment.state != 'draft' AND
  inredh_analytic_proyecto.id = %s
ORDER BY
  inredh_analytic_proyecto.name,
  account_analytic_account.code,
  account_invoice."number" ASC;''' % obj.id)

            for line in cr.dictfetchall():
                analytic_account_name = "[%s] %s" % (line['CÓDIGO ANALÍTICA'], line['CUENTA ANALÍTICA'])
                sheet.write(row, 0, line['PROYECTO'], table_body)
                sheet.write(row, 1, analytic_account_name, table_body)
                sheet.write(row, 2, line['EMPRESA'], table_body)
                sheet.write(row, 3, line['CI/RUC'], table_body)
                sheet.write(row, 4, line['CÓDIGO DE FACTURA'], table_body)
                sheet.write(row, 5, line['NÚMERO DE FACTURA'], table_body)
                sheet.write(row, 6, line['FECHA DE FACTURA'], table_body)
                sheet.write(row, 7, line['NÚMERO DE PAGO'], table_body)
                sheet.write(row, 8, line['FECHA PAGO'], table_body)
                sheet.write(row, 9, line['DETALLE'], table_body)
                sheet.write(row, 10, line['TOTAL DETALLE'], table_body)
                sheet.write(row, 11, line['TOTAL FACTURA'], table_body)
                sheet.write(row, 12, line['TOTAL IMPUESTOS'], table_body)
                sheet.write(row, 13, line['TOTAL RET. IR'], table_body)
                sheet.write(row, 14, line['TOTAL RET. IVA'], table_body)
                row += 1

detailed_crossovered_budget_xlsx('report.detailed.crossovered.budget.xlsx',
                                 'crossovered.budget')
