# -*- coding: utf-8 -*-

from openerp import models, fields, api

import xlwt
from cStringIO import StringIO
import base64
from xlsxwriter.workbook import Workbook


class CrossoveredBudgetReport(models.TransientModel):
    _name = 'crossovered.budget.report'
    _description = 'XLSX Report of budget'

    date = fields.Date('Date from', required=True)
    date_to = fields.Date('Date to', required=True)
    data = fields.Binary('Excel report')
    name = fields.Char('File name', size=64)
    budget_id = fields.Many2one('crossovered.budget', 'Presupuesto', required=True)

    @api.constrains('date', 'date_to')
    def _check_dates(self):
        if self.date > self.date_to:
            raise UserWarning("Date from can't be greater than date to")


    @api.multi
    def print_excel_report(self):
        self.ensure_one()
        filename = 'Reporte ejecucion.xlsx'
        fp = StringIO()
        workbook = Workbook(fp)

        # Styles
        bold = workbook.add_format({'bold': True})
        # Header table
        table_header = workbook.add_format({'bold': True})
        table_header.set_bottom()
        table_header.set_top()
        table_header.set_left()
        table_header.set_right()
        # Table body
        table_body = workbook.add_format({})
        table_body.set_bottom()
        table_body.set_top()
        table_body.set_left()
        table_body.set_right()
        budget = self.env['crossovered.budget'].browse(self.budget_id.id)

        report_name = budget.name
        # One sheet by budget
        sheet = workbook.add_worksheet(report_name[:31])
        sheet.write(0, 0, budget.name, bold)
        sheet.write(1, 0, "Periodo:", bold)
        sheet.write(1, 1, "%s - %s" % (self.date, self.date_to))

        # Table Header
        sheet.write(3, 0, u"Posición presupuestaria", table_header)
        sheet.write(3, 1, u"Cuenta analítica", table_header)
        sheet.write(3, 2, u"Presupuestado", table_header)
        sheet.write(3, 3, u"Ejecutado", table_header)
        sheet.write(3, 4, u"Saldo", table_header)
        sheet.write(3, 5, u"% Ejecutado", table_header)

        # Table Body
        row = 4
        for line in budget.crossovered_budget_line:
            sheet.write(row, 0, line.general_budget_id.name_get()[0][1], table_body)
            sheet.write(row, 1, line.analytic_account_id.name_get()[0][1], table_body)
            sheet.write(row, 2, line.planned_amount, table_body)
            query = "select sum(amount) from account_analytic_line where date>=%s and date<=%s and account_id=%s"
            params = (self.date, self.date_to, line.analytic_account_id.id)
            self._cr.execute(query, params)
            executed = self._cr.fetchone()[0] or 0.0
            if executed:
                sheet.write(row, 3, round(executed, 2), table_body)
                sheet.write(row, 4, round(line.planned_amount - executed, 2), table_body)
                try:
                    sheet.write(row, 5, round(executed/line.planned_amount*100, 2), table_body)
                except ZeroDivisionError:
                    sheet.write(row, 5, round(0, 2), table_body)
            else:
                sheet.write(row, 3, 0.0, table_body)
                sheet.write(row, 4, round(line.planned_amount - executed, 2), table_body)
                sheet.write(row, 5, 0.0, table_body)
            row += 1
        workbook.close()
        fp.seek(0)
        data = base64.encodestring(fp.read())
        self.write({'data': data, 'name': filename})
        return {
            'name': 'Crossovered Budget Report',
            'res_model': 'crossovered.budget.report',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'nodestroy': True,
            'context': self._context,
            'res_id': self.id,
        }