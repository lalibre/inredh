# -*- coding: utf-8 -*-
{
    'name': "inredh_account_budget",

    'summary': """
        Mejoras al budget""",

    'description': """
        Muestra el saldo y el porcentaje de ejecución.
    """,

    'author': "jfinlay@riseup.net",
    'website': "http://www.lalibre.net",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Account',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['account_budget', 'report_xlsx', 'inredh_analytic'],

    # always loaded
    'data': [
        'views/report_detailed_budget.xml',
        'wizard/crossovered_budget_report_view.xml'
    ],
    # only loaded in demonstration mode
    'demo': [ ],
}