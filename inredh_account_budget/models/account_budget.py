# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime

from openerp.osv import fields, osv
from openerp.tools import ustr, DEFAULT_SERVER_DATE_FORMAT
from openerp.tools.translate import _
from openerp.exceptions import UserError


class crossovered_budget_lines(osv.osv):

    def _theo(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
                res[line.id] = float(line.practical_amount - line.planned_amount)
        return res

    def _perc(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            if line.planned_amount != 0.00:
                res[line.id] = float((line.practical_amount or 0.0) / line.planned_amount) * 100
            else:
                res[line.id] = 0.00
        return res

    def _prac_amt(self, cr, uid, ids, context=None):
        res = {}
        result = 0.0
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            acc_ids = [x.id for x in line.general_budget_id.account_ids]
            if not acc_ids:
                raise UserError(_("The Budget '%s' has no accounts!") % ustr(line.general_budget_id.name))
            date_to = context.get('wizard_date_to') or line.date_to
            date_from = context.get('wizard_date_from') or line.date_from
            if line.analytic_account_id.id:
                cr.execute("SELECT SUM(a.amount) FROM account_analytic_line a, account_move m, account_move_line ml  "
                           "WHERE a.account_id=%s AND (a.date between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd'))"
                           " AND a.general_account_id=ANY(%s)"
                           " AND ml.id = a.move_id "
                           " AND m.id = ml.move_id "
                           " AND m.state = 'posted'", (line.analytic_account_id.id, date_from, date_to,acc_ids,))
                result = cr.fetchone()[0]
            if result is None:
                result = 0.00
            res[line.id] = result
        return res

    def _prac(self, cr, uid, ids, name, args, context=None):
        res={}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = self._prac_amt(cr, uid, [line.id], context=context)[line.id]
        return res

    _inherit = "crossovered.budget.lines"
    _description = "Budget Line"
    _columns = {
        'theoritical_amount': fields.function(_theo, string='Saldo', type='float', digits=0),
        'percentage': fields.function(_perc, string='Achievement', type='float'),
        'practical_amount':fields.function(_prac, string='Practical Amount', type='float', digits=0),

    }

