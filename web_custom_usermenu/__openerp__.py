# -*- encoding: utf-8 -*-

{
    'name': "Modificaciones a Web User Menu",
    'description': "Mi propio Web User Menu",
    'summary': """Cambia los links y submenus del menu de usuario principal
    """,
    'version': '9.0.1.0.0',
    'author': "Jonathan Finlay <jfinlay@riseup.net>",
    'maintainer': 'Jonathan Finlay <jfinlay@riseup.net>',
    'website': 'http://www.lalibre.net',
    'license': 'AGPL-3',
    'category': 'Web',
    'depends': [
        'web',
    ],
    'data': [
        # 'views/account_view.xml',
    ],
    'qweb': [
        'static/src/xml/base.xml',
        'static/src/xml/dashboard.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'web': True,
}
